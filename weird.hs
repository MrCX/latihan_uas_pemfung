-- MENGHILANGKAN ANGKA DARI SETIAP STRING PADA LIST
--- return true jika ada karakter yang bernilai angka, contoh "w3e" pada char[1] adalah 3, 3 adalah digit, maka 3 akan di return false
notNumber x = x `notElem` "1234567890"

--- Kembalikan semua char yang bernilai true
numberRemoval = filter notNumber


-- MENHILANGAN YANG KALIMAT YANG TERDAPAT HURUF VOKAL LEBIH DARI 4
--- return true jika ada karakter yang adalah huruf vokal maka akan dinilai true
isVowel x = x `elem` "aiueoAIUEO"

--- hitung jumlah karakter yang bernilai true, dimana karakter tersebut adalah huruf vokal
countVowel = length . filter isVowel

--- Untuk memberikan return true atau fals dari setiap masukan
isThreeOrLessVowels str = countVowel str < 4

---- pada input masukkan filter isThreeOrLessVowels ["ab3c", "bananae", "fuzzy", "c1c2"]
---- maka akan mengeluarkan ["ab3c","fuzzy","c1c2"]
---- dikarenakan "bananae" bernilai false


-- MENGHILANGKAN KALIMAT YANG MEMILIKI KEMUNCULAN KARAKTER YANG SAMA SEBANYAK 2 KALI ATAU LEBIH

isNotAppearTwiceInARow (a:b:ls) | (a==b)   = False --- Best case dimana urutan karakter pertama dan kedua sama, maka return false
isNotAppearTwiceInARow (a:ls)              = isNotAppearTwiceInARow ls --- Fungsi rekursif untuk cek karakter selanjutnya
isNotAppearTwiceInARow []                  = True --- Ketika list berisi kosong, maka telah dicek bahwa tidak ada yang karakternya sama, lalu return true


-- FUNGSI UTAMA
weirdFilter = filter isThreeOrLessVowels -- untuk menghilangkan kalimat, makanya menggunakan filter
            . filter isNotAppearTwiceInARow -- untuk menghilangkan kalimat, makanya menggunakan filter
            . map numberRemoval -- menggunakan map karena akan mengembalikan ke list kembali